

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/Git"
Import-Module -Name "$PrefixDir/Modules/Process"

$gitdir = Get-CurrentGitDir

if ($LASTEXITCODE -ne 0) {
    exit $LASTEXITCODE
}


$result = ProcessArgvDir -FilePath git -ArgumentList $args -Workdir "$gitdir/secure"
exit $result