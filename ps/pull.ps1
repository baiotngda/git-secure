#### pull from remote


$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/AesProvider"
Import-Module -Name "$PrefixDir/Modules/Git"
Import-Module -Name "$PrefixDir/Modules/Process"

$gitfiles = @(
    ".gitmodules",
    ".gitignore",
    ".gitattributes"
)

$workdir = Get-WorktreeDir
$gitdir = Get-CurrentGitDir -Worktree $workdir


Pop-Location

Set-Location "$gitdir/secure"
$op = Get-Content -ErrorAction Ignore -Path "$gitdir/pointer.json"|ConvertFrom-Json

$result = ProcessArgv -FilePath git -ArgumentList $args
if ($result -ne 0) {
    exit $result
}


$aeskey = git --git-dir=`"$gitdir/secure/.git`" config "aes.key" 

if ($null -eq $aeskey) {
    $aeskey = Read-Host "Please input your aes key"
}


if ($null -eq $op) {
    Write-Host "Resolve secure file"
    git ls-tree -r HEAD|ForEach-Object {
        $obj = -Split $_
        $file = $obj[3]
        $rawfile = "$workdir/$file"
        if ($gitfiles.Contains($file)) {
            Copy-Item "$gitdir/secure/$file" -Destination "$workdir/$file" -Force
            return ## ForEach-Object need return
        }
        $sdir = Split-Path -Path $rawfile
        $res = New-Item -ItemType Directory -Force -Path $sdir
        if ($null -eq $res) {
            Write-Host -ForegroundColor Red "create dir: $sdir"
            return ## ForEach-Object need return
        }
        $ret = Restore-AesFile -File $file -Key $aeskey -Destination $rawfile
        if ($ret -ne 0) {
            Write-Host -ForegroundColor Red "AES Decrypt $file failed"
        }
        else {
            Write-Host -ForegroundColor Green "Decrypt $file done"
        }
    }
}
else {
    Write-Host "trace new secure file"
    Get-Changeset -Commit $op.sid|ForEach-Object {
        $file = $_["path"]
        if ($_["status"] -eq "D") {
            Write-Host "Delete $file"
            Remove-Item -Path "$workdir/$file" -Force -ErrorAction Ignore
            return ## ForEach-Object need return
        }
        if ($gitfiles.Contains($file)) {
            Remove-Item -Path "$workdir/$file" -Force -ErrorAction Ignore
            Copy-Item "$gitdir/secure/$file" -Destination "$workdir/$file" -Force
            return ## ForEach-Object need return
        }
        $ret = Restore-AesFile -File "$gitdir/secure/$file" -Key $aeskey -Destination "$workdir/$file"
        if ($ret -ne 0) {
            Write-Host  -ForegroundColor Red "AES decrypt file '$file' throw exception"
        }
        else {
            Write-Host -ForegroundColor Green "Decrypt $file done"
        }
    }
}


$scommit = git rev-parse HEAD
$message = git log --pretty=format:'%s' -n 1 $scommit

Set-Location "$workdir"
git add -A
if ($LASTEXITCODE -ne 0) {
    exit $LASTEXITCODE
}
&git commit -m "$message"
if ($LASTEXITCODE -ne 0) {
    exit $LASTEXITCODE
}
$wcommit = git rev-parse HEAD


$pointer = @{}
$pointer["sid"] = $scommit
$pointer["wid"] = $wcommit

ConvertTo-Json -InputObject $pointer|Out-File "$gitdir/pointer.json"

Push-Location
