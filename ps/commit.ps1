#### ---->

#
# git diff currentid between 

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/AesProvider"
Import-Module -Name "$PrefixDir/Modules/Git"
Import-Module -Name "$PrefixDir/Modules/Process"

$workdir = Get-WorktreeDir
$gitdir = Get-CurrentGitDir -Worktree $workdir

#$env:GIT_SECURE_COMMIT = "Execute"

Pop-Location
Set-Location $workdir

$result = ProcessArgv -FilePath git -ArgumentList $args
if ($result -ne 0) {
    exit $result
}

$aeskey = git --git-dir=`"$gitdir/secure/.git`" config "aes.key" 

if ($null -eq $aeskey) {
    $aeskey = Read-Host "Please input your aes key"
}


$op = Get-Content -ErrorAction Ignore -Path "$gitdir/pointer.json"|ConvertFrom-Json

$gitfiles = @(
    ".gitmodules",
    ".gitignore",
    ".gitattributes"
)


if ($null -eq $op) {
    Write-Host "Initialize secure commit"
    git ls-tree -r HEAD|ForEach-Object {
        $obj = -Split $_
        $file = $obj[3]
        $sfile = "$gitdir/secure/$file"
        if ($gitfiles.Contains($file)) {
            Copy-Item "$workdir/$file" -Destination "$gitdir/secure/$file" -Force
            return ## ForEach-Object need return
        }
        $sdir = Split-Path -Path $sfile
        $res = New-Item -ItemType Directory -Force -Path $sdir
        if ($null -eq $res) {
            Write-Host -ForegroundColor Red "create dir: $sdir"
            return ## ForEach-Object need return
        }
        $ret = New-AesFile -File $file -Key $aeskey -Destination $sfile
        if ($ret -ne 0) {
            Write-Host -ForegroundColor Red "AES failed"
        }
        else {
            Write-Host -ForegroundColor Green "Encrypt $file done"
        }
    }
}
else {
    Write-Host "Add commit to secure track"
    Get-Changeset -Commit $op.wid|ForEach-Object {
        $file = $_["path"]

        if ($_["status"] -eq "D") {
            Write-Host -ForegroundColor Yellow "Delete $file done"
            Remove-Item -Path "$gitdir/secure/$file" -Force -ErrorAction Ignore
            return ## ForEach-Object need return
        }

        if ($gitfiles.Contains($file)) {
            Write-Host -ForegroundColor Green "sync git file: $file"
            Remove-Item -Path "$gitdir/secure/$file" -Force -ErrorAction Ignore
            Copy-Item "$workdir/$file" -Destination "$gitdir/secure/$file" -Force
            return ## ForEach-Object need return
        }

        $ret = New-AesFile -File $file -Key $aeskey -Destination "$gitdir/secure/$file"
        if ($ret -ne 0) {
            Write-Host  -ForegroundColor Red "AES encrypt file '$file' throw exception"
        }
        else {
            Write-Host -ForegroundColor Green "Encrypt $file done"
        }
        # end block
    }
}

$worktreecommit = git rev-parse HEAD
$message = git log --pretty=format:'%s' -n 1 $worktreecommit



$CurrentDir = $PWD
Set-Location "$gitdir/secure"
&git add -A
if ($LASTEXITCODE -ne 0) {
    exit $LASTEXITCODE
}
&git commit -m "$message"
if ($LASTEXITCODE -ne 0) {
    exit $LASTEXITCODE
}
$securecommit = git rev-parse HEAD

Set-Location $CurrentDir


$pointer = @{}
$pointer["sid"] = $securecommit
$pointer["wid"] = $worktreecommit

ConvertTo-Json -InputObject $pointer|Out-File "$gitdir/pointer.json"
