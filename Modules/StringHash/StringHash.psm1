function Get-StringHash {
    param(
        [string]$Text,
        [ValidateSet("MD5", "SHA1", "SHA256", "SHA384", "SHA512")]
        [string]$Algorithm = "SHA256"
    )
    $StringBuilder = New-Object System.Text.StringBuilder
    switch ($Algorithm) {
        {$_ -eq "SHA1"} {
            $hasher = [System.Security.Cryptography.SHA1]::Create()
        } {$_ -eq "MD5"} {
            $hasher = [System.Security.Cryptography.MD5]::Create()
        } {$_ -eq "SHA256"} {
            $hasher = [System.Security.Cryptography.SHA256]::Create()
        } {$_ -eq "SHA384"} {
            $hasher = [System.Security.Cryptography.SHA384]::Create()
        } {$_ -eq "SHA512"} {
            $hasher = [System.Security.Cryptography.SHA512]::Create()
        }
    }
    $hasher.ComputeHash([System.Text.Encoding]::UTF8.GetBytes($Text))| ForEach-Object {
        [Void]$StringBuilder.Append($_.ToString("x2"))
    }
    $StringBuilder.ToString()
}