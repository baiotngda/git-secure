@echo off

if not exist "%ProgramFiles%\PowerShell" goto

for /f "delims=" %%i in ('dir /b /ad "%ProgramFiles%\PowerShell" ') do set new=%%i

set PSCore="%ProgramFiles%\PowerShell\%new%\powershell.exe"

if not exist %PSCore% goto Desktop

%PSCore% -NoProfile -NoLogo -ExecutionPolicy unrestricted -File "%~dp0git-secure.ps1" %*

goto EOF

:Desktop

Powershell -NoProfile -NoLogo -ExecutionPolicy unrestricted -File "%~dp0git-secure.ps1" %*

:EOF