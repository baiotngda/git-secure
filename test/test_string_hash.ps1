#!/usr/bin/env pwsh


$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/StringHash"

$sha1 = Get-StringHash -Text "test-string" -Algorithm "SHA1"
Write-Host "SHA1: $sha1"
