param(
    [string]$Path
)

Get-ChildItem -Path $Path -Exclude ".git" -Recurse|ForEach-Object {
    Write-Host $_.Name
}
