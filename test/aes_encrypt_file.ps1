
param(
    [string]$File,
    [string]$Dest
)

$PrefixDir = Split-Path -Parent $PSScriptRoot
Import-Module -Name "$PrefixDir/Modules/AesProvider"

$aeskey=Read-Host "Please input aes key"

New-AesFile  -File "$file" -Key $aeskey -Destination $Dest